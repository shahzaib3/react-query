import React from "react";
import { queryCache } from "react-query";
import Navbar from "./components/Navbar";
import Planets from "./components/Planets";
import People from "./components/People";
import { ReactQueryDevtools } from "react-query-devtools";
import CreatePlanets from "./components/CreatePlanets";

function App() {
  const [page, setPage] = React.useState("planets");

  const handlePageNavigation = (page) => {
    setPage(page);
  };

  React.useEffect(() => {
    //fetching cache from local storage and pull into cache with cache keys
    const queriesWithData = JSON.parse(localStorage.getItem("queries"));

    if (!!queriesWithData) {
      queriesWithData.forEach((query) => {
        const [_queryKey, _queryIndex] = query.queryKey;

        queryCache.setQueryData([_queryKey, _queryIndex], query.data);
      });
    }
  }, []);

  return (
    <>
      <div className="App">
        <div style={{ flex: 5 }}>
          <h1>Star Wars Info</h1>
          <Navbar handlePageNavigation={handlePageNavigation} />
          <div className="content">
            {page === "planets" ? <Planets /> : <People />}
          </div>
        </div>
        <CreatePlanets />
        <ReactQueryDevtools initialIsOpen={false} />
      </div>
    </>
  );
}

export default App;

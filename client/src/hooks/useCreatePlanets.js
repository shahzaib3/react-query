import { useMutation, useQueryCache } from "react-query";
import { fetchWrapper } from "../restApi";
import { saveToLocalStorage } from "../utils";

export default function useCreatePlanets() {
  const queryCache = useQueryCache();
  const randomID = Math.floor(Math.random(0, 1) * 10);

  return useMutation(
    async (body) =>
      await fetchWrapper(
        "POST",
        `http://localhost:8080/api/createPlanets`,
        body
      ).then((res) => {
        return res;
      }),
    {
      onMutate: (values) => {
        //Its not implemented, testing get and set cache and save into local storage
        const previousPlanets = queryCache.getQueryData([
          "planetsCache",
          randomID,
        ]);

        queryCache.setQueryData(["planetsCache", randomID], (old) => ({
          ...old,
          ...values,
        }));

        return () =>
          queryCache.setQueryData(["planetsCache", randomID], previousPlanets);
      },
      //need to rollback on getting error to previous state of cache
      onError: (error, values, rollback) => rollback(),
      onSuccess: async () => {
        await queryCache.refetchQueries(["planets", 1]);
        //save cache to localStorage
        await saveToLocalStorage();
      },
    }
  );
}

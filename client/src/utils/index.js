import { queryCache } from "react-query";

export function sleep(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

export const saveToLocalStorage = () => {
  const queriesWithData = Object.values(queryCache.queries).map((query) => ({
    queryKey: query.queryKey,
    data: query.state.data,
  }));

  localStorage.setItem("queries", JSON.stringify(queriesWithData));
};

import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import {
  ReactQueryConfigProvider,
  QueryCache,
  ReactQueryCacheProvider,
} from "react-query";
import App from "./App";
import "./index.css";
import Test from "./components/Test";

// const queryConfig = {
//   queries: {
//     staleTime: 0,
//     cacheTime: 10,
//   },
// };

const queryCache = new QueryCache({
  defaultConfig: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

ReactDOM.render(
  //commneted for now use default cache time and stale time
  // <ReactQueryCacheProvider queryCache={queryCache}>
  //   <ReactQueryConfigProvider config={queryConfig}>
  <React.StrictMode>
    <Router>
      <div className="app__header">
        <Link to="/">Home</Link>
        <Link to="/test">test</Link>
      </div>

      <Switch>
        <Route exact path="/">
          <App />
        </Route>
        <Route exact path="/test">
          <Test />
        </Route>
      </Switch>
    </Router>
  </React.StrictMode>,
  //   </ReactQueryConfigProvider>
  // </ReactQueryCacheProvider>,
  document.getElementById("root")
);

import React from "react";

import useCreatePlanets from "../hooks/useCreatePlanets";

//This component was created to test refetch queries and Invalidate Queries
function CreatePlanets() {
  const [name, setName] = React.useState("");
  const [population, setPopulation] = React.useState("");
  const [terrain, setTerrain] = React.useState("");

  const [createPlanets, { status: createPlanetStatus }] = useCreatePlanets();

  const submitPlanets = async (e) => {
    e.preventDefault();
    const body = { name, population, terrain };
    await createPlanets(body);
  };

  return (
    <div className="planets__Form">
      <form onSubmit={submitPlanets}>
        <input
          value={name}
          onChange={(e) => setName(e.target.value)}
          type="text"
          name="name"
          className=""
        />
        <input
          type="population"
          name="population"
          value={population}
          onChange={(e) => setPopulation(e.target.value)}
          className="input__class"
        />
        <input
          type="terrain"
          name="terrain"
          value={terrain}
          onChange={(e) => setTerrain(e.target.value)}
          className="input__class"
        />

        <button type="submit" disabled={!name || !population || !terrain}>
          Create Planets
        </button>
      </form>
    </div>
  );
}

export default CreatePlanets;

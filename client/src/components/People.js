import React from "react";
import { usePaginatedQuery, useQuery } from "react-query";
import Person from "./Person";
import { fetchWrapper } from "../restApi";

const getPeoppleData = async (_, page) => {
  const peoppleData = await fetchWrapper(
    `https://swapi.dev/api/planets/?page=${page}`
  );
  return peoppleData;
};

function Peopple() {
  const [page, setPage] = React.useState(1);

  const { resolvedData, latestData, status } = usePaginatedQuery(
    ["peopple", page],
    getPeoppleData
  );

  const { results: peopleData } = resolvedData || {};

  const handlePrevPage = () => {
    setPage((prevPage) => Math.max(prevPage - 1, 1));
  };

  const handleNextPage = () => {
    const nextIndexPage = !latestData || !latestData.next ? page : page + 1;
    setPage(nextIndexPage);
  };

  return (
    <div>
      <h2>People</h2>
      <button disabled={page === 1} onClick={() => handlePrevPage()}>
        Previous
      </button>
      {page}
      <button
        disabled={!latestData || !latestData.next}
        onClick={() => handleNextPage()}
      >
        Next
      </button>
      {status === "loading" && <div> Loading....</div>}

      {status === "error" && <div> Error Fetching data ....</div>}

      {status === "success" && (
        <div>
          {peopleData?.map((person, key) => (
            <Person person={person} key={key} />
          ))}
        </div>
      )}
    </div>
  );
}

export default Peopple;

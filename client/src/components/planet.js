import React from "react";

function planet({ planet: { population, name, terrain } }) {
  return (
    <div className="card">
      <h3>{name}</h3>
      <p>Population - {population}</p>
      <p>Terrain - {terrain}</p>
    </div>
  );
}

export default planet;

import React from "react";
import { useQuery } from "react-query";
import Planet from "./planet";
import { fetchWrapper } from "../restApi";

const getPlanetsData = async () => {
  const planetsData = fetchWrapper(`http://localhost:8080/api/getPlanets`);

  return planetsData;
};

function Planets() {
  const [page, setPage] = React.useState(1);

  const { data, status } = useQuery(["planets", 1], getPlanetsData);

  const { results: planets } = data || {};

  return (
    <div>
      <h2>Planets</h2>

      <div className="warStars__status">
        {status === "loading" && <div> Loading....</div>}
        {status === "error" && <div> Error Fetching data ....</div>}
      </div>
      {status === "success" && (
        <div>
          {planets &&
            planets.map((planet, key) => <Planet planet={planet} key={key} />)}
        </div>
      )}
    </div>
  );
}

export default Planets;

import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
const styles = (theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
});

const Container = (props) => {
  return <Grid container spacing={4} {...props} />;
};
const Item = (props) => <Grid item {...props} />;

const FixedColumnLayout = withStyles(styles)(({ classes, width }) => (
  <div className={classes.root}>
    <Container>
      <Item xs={width}>
        <Paper className={classes.paper}>xs={width}</Paper>
      </Item>
      <Item xs={width}>
        <Paper className={classes.paper}>xs={width}</Paper>
      </Item>
      <Item xs={width}>
        <Paper className={classes.paper}>xs={width}</Paper>
      </Item>
      <Item xs={width}>
        <Paper className={classes.paper}>xs={width}</Paper>
      </Item>
    </Container>
  </div>
));
export default FixedColumnLayout;

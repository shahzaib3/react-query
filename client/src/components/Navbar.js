import React from "react";

function Navbar({ handlePageNavigation }) {
  return (
    <nav>
      <button className="text" onClick={() => handlePageNavigation("planets")}>
        Planets
      </button>
      <button
        onClick={() => {
          handlePageNavigation("people");
        }}
      >
        People
      </button>
    </nav>
  );
}

export default Navbar;

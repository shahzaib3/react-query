const config = require("./config");
const express = require("express");
const fs = require("fs");
const cors = require("cors");
const bodyParser = require("body-parser");

const app = express();

app.use(cors());
app.use(express.static("public"));
app.use(bodyParser.json());

app.get("/test", (req, res) => {
  res.send("Its working!!!");
});

app.get("/api/getPlanets", (req, res) => {
  const dummyData = fs.readFileSync("./testData.json");
  const data = JSON.parse(dummyData);
  res.send(data);
});

app.post("/api/createPlanets", (req, res) => {
  const body = req.body;

  const dummyData = fs.readFileSync("./testData.json");
  const data = JSON.parse(dummyData);
  const id = Math.floor(Math.random(10, 1000) * 1000);

  body.id = id;

  const isPlanetsAlreadyExists = data.results.some((planet) => {
    if (planet.id === id) {
      return true;
    }

    return false;
  });

  if (isPlanetsAlreadyExists) {
    res.send("already exist");
  }

  const finalResult = { ...data, results: [...data.results, body] };

  fs.writeFileSync("./testData.json", JSON.stringify(finalResult, null, 2));

  res.send(body);
});

app.listen(config.port, () =>
  console.log(`app  is listening on port ${config.port}`)
);
